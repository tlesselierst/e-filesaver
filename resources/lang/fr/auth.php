<?php

return [

	/*
		    |--------------------------------------------------------------------------
		    | Authentication Language Lines
		    |--------------------------------------------------------------------------
		    |
		    | The following language lines are used during authentication for various
		    | messages that we need to display to the user. You are free to modify
		    | these language lines according to your application's requirements.
		    |
	*/

  'failed' => 'Ces informations d\'identifications ne correspondents pas à nos données.',
  'throttle' => 'Il y a eu trop de tentatives. Veuillez attendre :seconds secondes.',
  'login.pagetitle' => 'S\'identifier',
  'login.label.email' => 'Adresse e-mail',
  'login.label.password' => 'Mot de passe',
  'login.label.rememberme' => 'Se souvenir de moi',
  'login.button.submit' => 'S\'identifier',
  'login.link.lostpassword' => 'J\'ai oublié mon mot de passe',
  'reset.pagetitle' => 'Demande de nouveau mot de passe',
  'reset.label.email' => 'Adresse e-mail',
  'reset.button.submit' => 'Envoyez-moi l\'e-mail pour changer le mot de passe',
];
