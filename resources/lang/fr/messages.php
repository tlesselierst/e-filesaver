<?php

return [

	/*
		    |--------------------------------------------------------------------------
		    | Messages Language Lines
		    |--------------------------------------------------------------------------
		    |
		    | The following language lines are used by the messages.
		    |
	*/

	'changed_params' => 'Vous avez changé le paramètre :key avec la valeur :newvalue',
	'user_created' => 'Un utilisateur a été ajouté',
	'user_deleted' => 'L\'utilisateur a été efffacé',
	'user_updated' => 'L\'utilisateur a été mis à jour',
	'avatar_deleted' => 'L \'avatar a été éffacé',
	'ftpsync_requested' => 'Demande de synchronisation FTP lancé',
	'synctoUP_requested' => 'REST Post vers UP initié',
	'file_deleted' => 'Le fichier a été effacé',
	'filesIn.file_exists' => 'Un fichier avec le même nom de fichier existe déjà. Le téléchargement a été annulé',
	'filesIn.file_upload_ok' => 'Téléchargement ok ',
	'filesIn.file_not_ok' => 'Le fichier n\'est pas valable ou n\'y est pas.',
	'filesIn.file_delete_ok' => 'Le fichier a été effacé!',
	'help.thank_you' => 'Merci! Nous traitons votre demande au plus tôt.',
];
