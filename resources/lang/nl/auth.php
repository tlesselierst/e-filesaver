<?php

return [

	/*
		    |--------------------------------------------------------------------------
		    | Authentication Language Lines
		    |--------------------------------------------------------------------------
		    |
		    | The following language lines are used during authentication for various
		    | messages that we need to display to the user. You are free to modify
		    | these language lines according to your application's requirements.
		    |
	*/

  'failed' => 'Gebruikersnaam en wachtwoord komen niet overeen met de informatie die opgeslagen is.',
  'throttle' => 'Teveel pogingen om in te loggen. U moet :seconds seconden wachten.',
  'login.pagetitle' => 'Inloggen',
  'login.label.email' => 'E-mail',
  'login.label.password' => 'Wachtwoord',
  'login.label.rememberme' => 'Onthou mij',
  'login.button.submit' => 'Aanloggen',
  'login.link.lostpassword' => 'Wachtwoord kwijt',
  'reset.pagetitle' => 'Vraag een nieuw wachtwoord aan',
  'reset.label.email' => 'E-mail',
  'reset.button.submit' => 'Stuur de email op met de instructies voor het wachtwoord',
];
