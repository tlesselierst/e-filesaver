<?php

return [

	/*
		    |--------------------------------------------------------------------------
		    | Messages Language Lines
		    |--------------------------------------------------------------------------
		    |
		    | The following language lines are used by the messages.
		    |
	*/

	'changed_params' => 'U heeft de parameter <b>:key</b> aangepast naar <b><i>:newvalue</i></b>',
	'user_created' => 'Gebruiker werd aangemaakt',
	'user_deleted' => 'Gebruiker werd verwijderd',
	'user_updated' => 'Gebruiker werd aangepast',
	'avatar_deleted' => 'De avatar werd verwijderd',	
	'ftpsync_requested' => 'FTP synchronisatie aanvraag uitgevoerd',
	'synctoUP_requested' => 'REST Post naar UP aangevraagd',
	'file_deleted' => 'Het bestand werd verwijderd',
	'filesIn.file_exists' => 'Er bestaat reeds een bestand met dezelfde naam. Upload werd gestaakt.',
	'filesIn.file_upload_ok' => 'Uploaden gelukt ',
	'filesIn.file_not_ok' => 'Er is geen bestand of het bestand is niet correct',
	'filesIn.file_delete_ok' => 'Het bestand werd verwijderd',
	'help.thank_you' => 'Bedankt! We proberen uw vraag zo spoedig mogelijk te beantwoorden',
];
