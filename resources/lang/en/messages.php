<?php

return [

	/*
		    |--------------------------------------------------------------------------
		    | Messages Language Lines
		    |--------------------------------------------------------------------------
		    |
		    | The following language lines are used by the messages.
		    |
	*/

	'changed_params' => 'You have changed :key to :newvalue',
	'user_created' => 'User correctly created',
	'user_deleted' => 'The user has been deleted!',
	'user_updated' => 'The users has been updated',
	'avatar_deleted' => 'The avatar has been deleted',
	'ftpsync_requested' => 'STP synchronisation request was launched',
	'synctoUP_requested' => 'REST Post to UP requested',
	'file_deleted' => 'File was deleted!',
	'filesIn.file_exists' => 'A file with the same filename is already present. Upload aborted',
	'filesIn.file_upload_ok' => 'Upload of successfully',
	'filesIn.file_not_ok' => 'uploaded file is not valid or missing',
	'filesIn.file_delete_ok' => 'File successfully deleted!',
	'help.thank_you' => 'Thank you for your message. It has been sent.',
];
