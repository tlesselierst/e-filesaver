<?php

return [

	/*
		    |--------------------------------------------------------------------------
		    | Authentication Language Lines
		    |--------------------------------------------------------------------------
		    |
		    | The following language lines are used during authentication for various
		    | messages that we need to display to the user. You are free to modify
		    | these language lines according to your application's requirements.
		    |
	*/

  'failed' => 'These credentials do not match our records.',
  'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
  'login.pagetitle' => 'Login',
  'login.label.email' => 'E-mail',
  'login.label.password' => 'Password',
  'login.label.rememberme' => 'Remember me',
  'login.button.submit' => 'Log on',
  'login.link.lostpassword' => 'I forgot my password',
  'reset.pagetitle' => 'Request a new password',
  'reset.label.email' => 'E-mail',
  'reset.button.submit' => 'Send the reset password email',
];
