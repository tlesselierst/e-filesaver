@extends('layouts.admin', array('page_title' =>  trans('app.filesOut.pagetitle') ,
                                'breadcrumbs' => 'files_out'  ))

@section('content')
     <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <div class="row">
              <div class="col-md-6">
                <h3 class="panel-title">{{ trans('app.filesOut.formtitle')}}</h3>
              </div>
              <div class="col-md-6">
                <div class="btn-toolbar">
                 <a class="btn btn-sm btn-default pull-right" href="{{url('/readftp')}}"> <i class="fa fa fa-refresh"></i>&nbsp; {{ trans('app.filesOut.button.read')}}</a>

                @if ($allow_create_fb == true )
                  <button type="button" class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#modal-file-upload">
                    <i class="fa fa-plus-square-o"></i>&nbsp; {{ trans('app.filesOut.button.upload')}}
                  </button>
                @endif
                </div>
              </div> 
            </div>
          </div>
          <div class="panel-body">
    @include('partials.session_messages')
     <div class="panel panel-default">
      <div class="panel-body">
        {!! trans('app.filesOut.explanation') !!}
      </div>
    </div>
      {!! $dataTable->table() !!}
</div>
        </div>
      </div>
    </div>
  </div>
  @include('filesOut._modals')
@endsection

@push('scripts')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
{!! $dataTable->scripts() !!}
@endpush

@section('jsscripts')
<script>
  //confirm file delete
  function delete_file(name, id) {
    $("#delete-file-name1").html(name);
    $("#modal-file-delete").modal("show");

    var url = "/filesOut/Delete/" + id;

    document.getElementById("confirm-delete").setAttribute('action', url);

  }

</script>
@stop

