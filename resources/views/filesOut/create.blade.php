@extends('layouts.admin', array('page_title' =>  trans('app.filesOut.pagetitle') ,
                                'breadcrumbs' => 'files_out'  ))

@section('content')
     <div class="container-fluid">

    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">{{ trans('app.filesOut.formtitle')}}</h3>
          </div>
          <div class="panel-body">

    @include('partials.session_messages')

    {!! Form::open(array('url'=>'filesOut','method'=>'POST', 'files'=>true)) !!}
		<div class="control-group">
			<div class="controls">
		    	{!! Form::file('file') !!}
		    </div>
		</div>

        <hr>
    	<!-- submit buttons -->
  		{{ Form::submit(trans('app.filesOut.button.upload'), array('class' => 'btn btn-primary')) }}
         {{ link_to(URL::previous(), trans('app.filesOut.button.back'), ['class' => 'btn btn-default']) }}
    {!! Form::close() !!}
    </div>
        </div>
      </div>
    </div>
  </div>
@endsection

