<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ trans('app.appname')}}</title>

    <!-- Styles -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" >
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css" >
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
   <link rel="stylesheet" href="{{ asset('css/efilesaver.css')}}" >
    <!-- Fonts -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel='stylesheet' type='text/css' >


     <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>
<body id="app-layout">
    @include('partials.nav')

    <div class="container">
        @yield('content')
    </div>


    <footer class="footer">
      <div class="container">
        <div class="row">
            <div class="col-md-2">&copy; T.L.E. - 2016</div>
            <div class="col-md-1 pull-right">v1.12</div>
        </div>
      </div>
    </footer>
    <!-- JavaScripts -->
    <!-- jQuery -->
    <!--script src="//code.jquery.com/jquery.js"></script -->
    <script src="{{ asset('js/jquery.min.js') }}" type="text/javascript"></script>

    <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script >
    <!-- Bootstrap JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script >
    <!-- App scripts -->
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
    @stack('scripts')
    @yield('jsscripts')
    <script>
        window.setTimeout(function() {
            $("#success-alert").fadeTo(500, 0).slideUp(500, function(){
                $(this).remove();
            });
        }, 4000);
    </script>
</body>
</html>
