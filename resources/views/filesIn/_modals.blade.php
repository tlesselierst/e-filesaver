{{--  Delete file Modal --}}
<div class="modal fade" tabindex="-1" id="modal-file-delete" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ trans('app.files.delete.formtitle')}}</h4>
      </div>
      <div class="modal-body">
        <p class="lead">
        <i class="fa fa-question-circle fa-lg"></i> &nbsp;
          {{ trans('app.files.delete.areyousure')}}  <kbd><span id="delete-file-name1">file</span></kbd></p>
      </div>
      <div class="modal-footer">
        <form id="confirm-delete" method="POST" action="#" >
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="_method" value="DELETE">
          <input type="hidden" name="id" id="delete-file-id">
          <button type="button" class="btn btn-default btn-md" data-dismiss="modal"> {{ trans('app.files.delete.button.cancel')}} </button>
          <button type="submit" class="btn btn-danger btn-md"><i class="fa fa-times-circle"></i>
      &nbsp; {{ trans('app.files.delete.button.delete')}}
      </button>
    </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

{{--  Upload File Modal --}}
<div class="modal fade" id="modal-file-upload">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="POST" action="filesIn" enctype="multipart/form-data" class="form-horizontal" files="true">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">{{ trans('app.files.upload.formtitle')}}</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="file" class="col-sm-3 control-label">{{ trans('app.files.upload.label.file')}}</label>
            <div class="col-sm-8">
              <input type="file" id="file" name="file">
            </div>
          </div>
          <div class="form-group">
            <label for="file_name" class="col-sm-3 control-label">{{ trans('app.files.upload.label.optional_name')}}</label>
            <div class="col-sm-8">
              <input type="text" id="file_name" name="file_name" class="form-control">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-md" data-dismiss="modal">{{trans('app.files.upload.button.back')}}</button>
          <button type="submit" class="btn btn-primary btn-md"><i class="fa fa-upload"></i>
      &nbsp; {{trans('app.files.upload.button.upload')}}</button>
      </div>
    </form>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->