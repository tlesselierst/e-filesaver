@extends('layouts.admin', array('page_title' =>  trans('app.filedetail.pagetitle', ['filename' =>  $record->filename]) ,
                                'breadcrumbs' => 'files_in'  ))

@section('content')
     <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <div class="row">
              <div class="col-md-6">
            </div>
            <div class="col-md-6">
                <div class="btn-toolbar">
                  <a class="btn btn-sm btn-default pull-right" href="{{URL::previous()}}"> <i class="fa fa fa-arrow-left"></i>&nbsp; {{ trans('app.filedetail.button.back')}}</a>
                </div>
            </div>
          </div>
          <div class="panel-body">
      <div class="jumbotron">
      <ul>
      	<li>{{ trans('app.filedetail.label.filename', ['filename' =>  $record->filename])}}</li>
      	<li>{{ trans('app.filedetail.label.filetype', ['filetype' =>  $record->filetype ])}}</li>
      	<li>{{ trans('app.filedetail.label.filesize', ['filesize' => $record->filesize ])}}</li>
      	<li>{{ trans('app.filedetail.label.origin', ['origin' => $record->origin])}} </li>
        <li>{{ trans('app.filedetail.label.path', ['file_path' => $record->file_path])}} </li>

      	<li> <a href='{{ url('filesIn/download/' .$record->id) }}'> {{trans('app.filedetail.button.download')}}</a></li>
      </ul>
      </div>
  <hr>
  
 </div>
        </div>
      </div>
    </div>
  </div>
@endsection