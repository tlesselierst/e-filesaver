@extends('layouts.admin', array('page_title' =>  trans('app.pages.support.pagetitle'),
                                'breadcrumbs' => 'home'))

@section('content')
  <div class="container-fluid">

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#question" aria-controls="question" role="tab" data-toggle="tab">{{ trans('app.pages.support.formtitle')}}</a></li>
    <li role="presentation"><a href="#faq" aria-controls="faq" role="tab" data-toggle="tab">{{ trans('app.pages.support.faq')}}</a></li>

  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="question"><!-- Question Pane -->
      <div class="row">
        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading">
            <form action="/contact" method="post" class="form-horizontal">
              <input type="hidden" name="_token" value="{!! csrf_token() !!}">
              <div class="row">
                <div class="col-md-10">
                  <h3 class="panel-title">{{ trans('app.pages.support.introtext')}}</h3>
                </div>
                <div class="col-md-2">
                  <div class="btn-toolbar">
                    <button type="submit" class="btn btn-primary btn-sm pull-right"> <i class="fa fa-envelope-o"></i>&nbsp; {{ trans('app.pages.support.button.submit')}}</button>
                  </div>
                </div>
                <br>
              </div>
              <div class="panel-body">
              @include('partials.session_messages')
                <div class="row control-group">
                  <div class="form-group">
                    <label for="name" class="col-sm-3 control-label">{{ trans('app.pages.support.label.name')}}</label>
                     <div class="col-sm-8">
                      <input type="text" class="form-control" id="name" name="name">
                    </div>
                  </div>
                </div>

                <div class="row control-group">
                  <div class="form-group">
                    <label for="email"  class="col-sm-3 control-label">{{ trans('app.pages.support.label.email')}}</label>
                     <div class="col-sm-8">
                      <input type="email" class="form-control" id="email" name="email">
                    </div>
                  </div>
                </div>

                <div class="row control-group">
                  <div class="form-group">
                    <label for="phone"  class="col-sm-3 control-label">{{ trans('app.pages.support.label.phone')}}</label>
                     <div class="col-sm-8">
                      <input type="tel" class="form-control" id="phone" name="phone">
                    </div>
                  </div>
                </div>

                <div class="row control-group">
                  <div class="form-group">
                    <label for="message"  class="col-sm-3 control-label">{{ trans('app.pages.support.label.message')}}</label>
                    <div class="col-sm-8">
                      <textarea rows="5" class="form-control" id="message" name="message"></textarea>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div><!-- end of question panel -->

    <div role="tabpanel" class="tab-pane" id="faq"><!-- FAQ panel -->
      <div class="row">
        <div class="col-sm-12">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">{{ trans('app.pages.support.faq')}}</h3>
            </div>
            <div class="panel-body" style="min-height: 400px;">
                <iframe frameborder="0" style="position: relative; height: 400px; width: 100%;" src="{{ trans('app.pages.support.faq.url') }}"'"></iframe>
            </div>
          </div>
        </div>
      </div>
    </div><!-- end of FAQ panel -->

  </div><!-- end of panels -->
</div>

@endsection