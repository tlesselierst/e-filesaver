@extends('layouts.app')

@section('content')
<section id="slider"><!--slider-->
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img src="images/techno.jpg" alt="techno">
      <div class="carousel-caption">
          <h1>{{ trans('app.home.slide1.title')}}</h1>
          <h2>{{ trans('app.home.slide1.subtitle')}}</h2>
          <p>{{ trans('app.home.slide1.text')}}</p>
          <button type="button" class="btn btn-default get">{{ trans('app.home.slide1.button')}}</button>
        </div>
    </div>

    <div class="item">
      <img src="images/up.jpg" alt="no time">
          <div class="carousel-caption">
          <h1>{{ trans('app.home.slide2.title')}}</h1>
          <h2>{{ trans('app.home.slide2.subtitle')}}</h2>
          <p>{{ trans('app.home.slide2.text')}}</p>
          <button type="button" class="btn btn-default get">{{ trans('app.home.slide2.button')}}</button>
        </div>
    </div>

    <div class="item">
      <img src="images/validate.jpg" alt="Easy config">
      <div class="carousel-caption">
          <h1>{{ trans('app.home.slide3.title')}}</h1>
          <h2>{{ trans('app.home.slide3.subtitle')}}</h2>
          <p>{{ trans('app.home.slide3.text')}}</p>
        </div>
    </div>
  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">{{ trans('app.home.previous')}}</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">{{ trans('app.home.next')}}</span>
  </a>
</div>
</div>
</div>
</div>
</section>
@endsection