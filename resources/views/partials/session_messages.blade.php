@if ($errors->any())
<div class="alert alert-danger alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>{{ trans('validation.message.title')}}</strong>
    {{ trans('validation.message.intro')}}<br><br>
    <ul>
      @foreach($errors->all() as $error)
      <li>{{$error}}</li>
      @endforeach
    </ul>
</div>
@endif
@if (Session::has('success'))
  <div class="alert alert-success alert-dismissible fade in" id="success-alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <strong>
      <i class="fa fa-check-circle fa-lg fa-fw"></i>Success &nbsp;
    </strong>
    {{Session::get('success')}}
  </div>
@endif