<!-- Main Header -->
<header class="main-header">
  <!-- Logo -->
  <a href="{{ url('/admin/dashboard') }}" class="logo">{{ trans('app.menu.appname')}}</a>

  <!-- Header Navbar -->
  <nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">


        <!-- Notifications Menu -->
        <li class="dropdown notifications-menu">
          <!-- Menu toggle button -->
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <i class="fa fa-bell-o"></i>
            <span class="label label-warning">10</span>
          </a>
          <ul class="dropdown-menu">
            <li class="header">You have 10 notifications</li>
            <li>
              <!-- Inner Menu: contains the notifications -->
              <ul class="menu">
                <li><!-- start notification -->
                  <a href="#">
                    <i class="fa fa-users text-aqua"></i> 5 new members joined today
                  </a>
                </li><!-- end notification -->
              </ul>
            </li>
            <li class="footer"><a href="#">View all</a></li>
          </ul>
        </li>
        <!-- User Account Menu -->
        <li class="dropdown user user-menu">
          <!-- Menu Toggle Button -->
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <!-- The user image in the navbar-->
            @if(!empty($user->avatar))
              <img src="/uploads/avatars/{{ $user->avatar }}" class="user-image" alt="User Image">
            @else 
              <img src="{{asset('images/default.jpg')}}" class="user-image" alt="User Image">
            @endif
            <!-- hidden-xs hides the username on small devices so only the image appears. -->
            <span class="hidden-xs">{{ Auth::user()->name }}</span>
          </a>
          <ul class="dropdown-menu">
            <!-- The user image in the menu -->
            <li class="user-header">
               @if(!empty($user->avatar))
              <img src="/uploads/avatars/{{ $user->avatar }}" class="img-circle" alt="User Image">
            @else 
              <img src="{{asset('images/default.jpg')}}" class="img-circle" alt="User Image">
            @endif
              <p>
                {{ Auth::user()->name }}
                <small>{{ trans('app.menu.membersince')}} {{  Carbon\Carbon::parse(Auth::user()->created_at)->format('d-M-Y') }}</small>
              </p>
            </li>
            <!-- Menu Body -->
             <li class="user-body">
             @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                <div class="col-xs-4 text-center">
                  <a rel="alternate" hreflang="{{$localeCode}}" href="{{LaravelLocalization::getLocalizedURL($localeCode) }}">
                   {{{ $properties['native'] }}}
                   </a>
                </div>
              @endforeach
            </li>
            <!-- Menu Footer-->
            <li class="user-footer">
              <div class="pull-left">
                <a href="{{ url('/profile') }}" class="btn btn-default btn-flat">Profile</a>
              </div>
              <div class="pull-right">
                <a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>{{ trans('app.menu.logout')}}</a>
              </div>
            </li>
          </ul>
        </li>
      </ul>
    </div>
  </nav>
</header>