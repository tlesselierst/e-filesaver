 <!-- Static navbar -->
    <nav class="navbar navbar-default navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">{{ trans('app.menu.toggle')}}</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/">{{ trans('app.menu.appname')}}</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav">
            <li><a href="{{ url('/') }}">{{ trans('app.menu.home')}}</a></li>
            @if (Auth::check())
            <li><a href="{{ url('/support') }}">{{ trans('app.menu.about')}}</a></li>
            <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ trans('app.menu.files') }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/filesIn') }}">{{ trans('app.menu.incoming.files')}} </a></li>
                                <li><a href="{{ url('/filesOut') }}">{{ trans('app.menu.outgoing.files')}} </a></li>
                            </ul>
            </li>
            @endif
             @if (Auth::check())
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{{ trans('app.menu.administration')}}<span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="{{ url('admin/settings') }}">{{ trans('app.menu.settings')}}</a></li>
                  <li><a href="{{ url('/admin/users') }}">{{ trans('app.menu.users')}}</a></li>
                </ul>
              </li>
            @endif
          </ul>
          <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">{{ trans('app.menu.login')}}</a></li>
                        <li><a href="{{ url('/register') }}">{{ trans('app.menu.register')}}</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>{{ trans('app.menu.logout')}}</a></li>
                            </ul>
                        </li>
                    @endif
                    <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{trans('app.menu.language') }} <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu" role="menu">
                              @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                <li>
                                  <a rel="alternate" hreflang="{{$localeCode}}" href="{{LaravelLocalization::getLocalizedURL($localeCode) }}">
                                    {{{ $properties['native'] }}}
                                  </a>
                                </li>
                              @endforeach
                            </ul>
                </ul>

        </div><!--/.nav-collapse -->
      </div>
    </nav>