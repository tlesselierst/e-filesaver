<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">

    <!-- Sidebar Menu -->
    <ul class="sidebar-menu">
      <!-- Optionally, you can add icons to the links -->
        <li>
          <a href="{{ url('/admin/dashboard') }}">
            <i class="logo-mini fa fa-dashboard"></i>
            <span>{{ trans('app.menu.dashboard')}}</span>
          </a>
        </li>
        @if (Auth::check())
          <li>
            <a href="{{ url('/support') }}">
              <i class="fa fa-book"></i>
              <span>{{ trans('app.menu.about')}}</span>
            </a>
          </li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-files-o"></i>
              <span>{{ trans('app.menu.files') }}</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li>
                <a href="{{ url('/filesIn') }}">
                  <i class="ion ion-ios-cloud-upload-outline"></i>
                  <span> {{ trans('app.menu.incoming.files')}}</span>
                </a>
              </li>
              <li>
                <a href="{{ url('/filesOut') }}">
                  <i class="ion ion-ios-cloud-download-outline"></i>
                  <span> {{ trans('app.menu.outgoing.files')}}</span> 
                </a>
              </li>
            </ul>
          </li>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-gears"></i>
              <span>{{ trans('app.menu.administration')}}</span>
              <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
              <li>
                <a href="{{ url('admin/settings') }}">
                   <i class="fa fa-gear"></i>
                  <span>{{ trans('app.menu.settings')}}</span>
                </a>
              </li>
              <li>
                <a href="{{ url('/admin/users') }}"> 
                   <i class="fa fa-users"></i>
                  <span>{{ trans('app.menu.users')}}</span>
                </a>
              </li>
            </ul>
          </li>
        @endif
    </ul><!-- /.sidebar-menu -->
  </section>
  <!-- /.sidebar -->
</aside>