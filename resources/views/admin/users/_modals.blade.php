{{--  Delete user Modal --}}
<div class="modal fade" tabindex="-1" id="modal-user-delete" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ trans('app.admin.users.delete.formtitle')}}</h4>
      </div>
      <div class="modal-body">
        <p class="lead">
        <i class="fa fa-question-circle fa-lg"></i> &nbsp;
          {{ trans('app.admin.user.delete.areyousure')}}  <kbd><span id="delete-user-name">file</span></kbd></p>
      </div>
      <div class="modal-footer">
        <form id="confirm-delete" method="POST" action="#" >
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="_method" value="DELETE">
          <button type="button" class="btn btn-default btn-md" data-dismiss="modal"> {{ trans('app.admin.user.delete.button.cancel')}} </button>
          <button type="submit" class="btn btn-danger btn-md"><i class="fa fa-times-circle"></i>
      &nbsp; {{ trans('app.admin.user.delete.button.delete')}}
      </button>
    </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

{{--  Create User Modal --}}
<div class="modal fade" id="modal-create-user">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <form method="POST" action="/admin/users" class="form-horizontal" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">{{ trans('app.admin.users.create.pagetitle') }}</h4>
        </div>
        <div class="modal-body">
            @include('admin.users._fields')
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-md" data-dismiss="modal">{{trans('app.admin.users.create.button.cancel')}}</button>
          <button type="submit" class="btn btn-primary btn-md"><i class="fa fa-upload"></i>
      &nbsp; {{trans('app.admin.users.create.button.submit')}}</button>
      </div>
    </form>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->