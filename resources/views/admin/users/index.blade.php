@extends('layouts.admin', array('page_title' =>  trans('app.admin.users.pagetitle'),
                                'breadcrumbs' => 'users'))

@section('content')
     <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <div class="row">
              <div class="col-md-6">
                <h3 class="panel-title">{{ trans('app.admin.users.formtitle')}}</h3>
              </div>
              <div class="col-md-6">
                <button type="button" class="btn btn-success btn-sm pull-right" data-toggle="modal" data-target="#modal-create-user">
                  <i class="fa fa-plus-square-o"></i>&nbsp; {{ trans('app.admin.users.button.new')}}
                </button>
              </div>
            </div>
          </div>
          <div class="panel-body">
    @include('partials.session_messages')

      {!! $dataTable->table() !!}

</div>
        </div>
      </div>
    </div>
  </div>
    @include('admin.users._modals')
@endsection

@push('scripts')
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.0.3/css/buttons.dataTables.min.css">
<script src="https://cdn.datatables.net/buttons/1.0.3/js/dataTables.buttons.min.js"></script>
<script src="/vendor/datatables/buttons.server-side.js"></script>
{!! $dataTable->scripts() !!}
@endpush

@section('jsscripts')
<script>
  //confirm user delete
  function delete_user(name, id) {
    $("#delete-user-name").html(name);
    $("#modal-user-delete").modal("show");
    var url = "/admin/users/" + id;
    document.getElementById("confirm-delete").setAttribute('action', url);
  };

</script>
<script type="text/javascript">
/**
 * File: js/showhide.js
 * Author: design1online.com, LLC
 * Purpose: toggle the visibility of fields depending on the value of another field
 **/
$(document).ready(function () {
    toggleFields(); //call this first so we start out with the correct visibility depending on the selected form values
    //this will call our toggleFields function every time the selection value of our underAge field changes
    $("#user_type").change(function () {
        toggleFields();
    });

});
//this toggles the visibility of our parent permission fields depending on the current selected value of the underAge field
function toggleFields() {
    if ($("#user_type").val() == 'REST')
        $("#api_token_section").show();
    else
        $("#api_token_section").hide();
}
</script>
@stop