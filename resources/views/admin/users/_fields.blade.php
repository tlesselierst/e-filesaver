<div class="row">
    <div class="col-md-1">
        @if(!empty($user->avatar))
            <img src="/uploads/avatars/{{ $user->avatar }}" style="width:150px; height:150px; float:left; border-radius:50%; margin-right:25px;">
        @else 
            <img src="{{asset('images/default.jpg')}}" style="width:150px; height:150px; float:left; border-radius:50%; margin-right:25px;">
        @endif
        
    </div>
     <div class="col-md-11">
        <div class="form-group">
            {{ Form::label('name', trans('app.admin.users.fields.name'), array('class' =>'col-sm-3 control-label')) }}
            <div class="col-sm-8">
            {{ Form::text('name', null, array('class' => 'form-control')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('email', trans('app.admin.users.fields.email'), array('class' =>'col-sm-3 control-label')) }}
             <div class="col-sm-8">
            {{ Form::email('email', null, array('class' => 'form-control')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('password', trans('app.admin.users.fields.password'), array('class' =>'col-sm-3 control-label')) }}
             <div class="col-sm-8">
            {{ Form::password('password', null, array('class' => 'form-control')) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('avatar', trans('app.admin.users.fields.avatar'), array('class' =>'col-sm-3 control-label')) }}
             <div class="col-sm-8">
            {{ Form::file('avatar', null, array('class' => 'form-control')) }}
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('user_type', trans('app.admin.users.fields.usertype'), array('class' =>'col-sm-3 control-label')) }}
             <div class="col-sm-8">
            {{ Form::select('user_type', array('0' => trans('app.admin.users.fields.ut.select'), 'UI' =>trans('app.admin.users.fields.ut.ui'), 'REST' => trans('app.admin.users.fields.ut.rest')), null, array('class' => 'form-control')) }}
            </div>
        </div>

        <div class="form-group" id="api_token_section">
                {{ Form::label('api_token',trans('app.admin.users.fields.api'), array('class' =>'col-sm-3 control-label')) }}
                 <div class="col-sm-8">
                {{ Form::text('api_token', null, array('class' => 'form-control')) }}
                </div>
        </div>
    </div>
</div>