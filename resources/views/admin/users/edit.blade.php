@extends('layouts.admin', array('page_title' =>  trans('app.admin.users.edit.pagetitle', ['user' => $user->name]),                  'breadcrumbs' => 'user_edit'))


@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          {{ Form::model($user, array('route' => array('admin.users.update', $user->id), 'method' => 'PUT', 'class' =>'form-horizontal', 'enctype' =>'multipart/form-data')) }}
          <div class="panel-heading">
            <div class="row">
              <div class="col-md-6">
              </div>
              <div class="col-md-6">
                <div class="btn-toolbar">
                  <button type="submit" class="btn btn-primary btn-md pull-right"><i class="fa fa-floppy-o"></i>&nbsp; {{trans('app.admin.users.edit.button.submit')}}</button>
                  <button type="button" class="btn btn-default btn-md pull-right" onclick="delete_avatar('{{$user->id}}')"><i class="fa fa-meh-o fa-lg"></i>&nbsp; {{trans('app.admin.users.edit.button.delete_avatar')}}</button>
                  <button type="button" class="btn btn-default btn-md pull-right"  onclick="goBack()"><i class="fa fa-arrow-circle-left fa-lg"></i>&nbsp; {{trans('app.admin.users.edit.button.back')}}</button>
                </div>
              </div>
            </div>
          </div>
          <div class="panel-body">
            @include('partials.session_messages')
            @include('admin.users._fields')
          </div>
          {{ Form::close() }}
        </div>
      </div>
    </div>
  </div>
  {{--  Delete user Modal --}}
<div class="modal fade" tabindex="-1" id="modal-avatar" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">{{ trans('app.admin.users.avatar.delete.formtitle')}}</h4>
      </div>
      <div class="modal-body">
        <p class="lead">
          <i class="fa fa-question-circle fa-lg"></i> &nbsp;
          {{ trans('app.admin.user.avatar.delete.areyousure')}} 
          </p>
      </div>
      <div class="modal-footer">
        <form id="confirm-delete" method="POST" action="#" >
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          <input type="hidden" name="_method" value="DELETE">
          <button type="button" class="btn btn-default btn-md" data-dismiss="modal"> {{ trans('app.admin.user.avatar.delete.button.cancel')}} </button>
          <button type="submit" class="btn btn-danger btn-md"><i class="fa fa-times-circle"></i>
      &nbsp; {{ trans('app.admin.user.avatar.delete.button.delete')}}
      </button>
    </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection

@section('jsscripts')
<script type="text/javascript">
/**
 * File: js/showhide.js
 * Author: design1online.com, LLC
 * Purpose: toggle the visibility of fields depending on the value of another field
 **/
$(document).ready(function () {
    toggleFields(); //call this first so we start out with the correct visibility depending on the selected form values
    //this will call our toggleFields function every time the selection value of our underAge field changes
    $("#user_type").change(function () {
        toggleFields();
    });

});
//this toggles the visibility of our parent permission fields depending on the current selected value of the underAge field
function toggleFields() {
    if ($("#user_type").val() == 'REST')
        $("#api_token_section").show();
    else
        $("#api_token_section").hide();
}
</script>

<script>
function goBack() {
    window.history.back();
}
</script>

<script>
  //confirm avatar delete
  function delete_avatar(id) {
    $("#modal-avatar").modal("show");
    var url = "/admin/avatar/" + id;
    document.getElementById("confirm-delete").setAttribute('action', url);
  };

</script>
@endsection