@extends('layouts.admin', array('page_title' => trans('app.admin.dashboard.pagetitle'),
                                'breadcrumbs' => 'dashboard'))
@section('content')
    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="ion ion-ios-cloud-upload-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">{{ trans('app.dashboard.label.filesin')}}</span>
              <span class="info-box-number">{{ $total_in }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="ion ion-ios-download-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">{{ trans('app.dashboard.label.filesinsent')}}</span>
              <span class="info-box-number">{{ $total_in_sent }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix visible-sm-block"></div>

        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-green"><i class="ion ion-ios-upload-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">{{ trans('app.dashboard.label.filesout')}}</span>
              <span class="info-box-number">{{ $total_out }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="ion ion-ios-cloud-download-outline"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">{{ trans('app.dashboard.label.filesoutsent')}}</span>
              <span class="info-box-number">{{ $total_out_sent }}</span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <div class="col-md-12">
          <div class="box">
            <div class="box-header with-border">
              <h3 class="box-title">{{ trans('app.dashboard.report.monthly.title')}}</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                @if ( $total_in > 0 || $total_out > 0)
                <div class="col-md-9">
                @else 
                <div class="col-md-12">
                @endif 
                  <p class="text-center">
                    <strong>{{ trans('app.dashboard.report.period', ['firstdate' => $min, 'enddate' => $max ])}}</strong>
                  </p>

                  <div class="chart">
                    <!-- Sales Chart Canvas -->
                    <canvas id="filesTxfrChart" style="height: 180px;"></canvas>
                  </div>
                  <!-- /.chart-responsive -->
                </div>
                <!-- /.col -->
                @if ( $total_in > 0 || $total_out > 0)
                <div class="col-md-3">
                  <p class="text-center">
                    <strong>{{ trans('app.dashboard.report.monthly.goal.title')}}</strong>
                  </p>

                  <!-- /.progress-group -->
                   @if ( $total_in > 0 )
                  <div class="progress-group">
                    <span class="progress-text">{{ trans('app.dashboard.report.monthly.goal.incoming.title')}}</span>
                    <span class="progress-number"><b>{{ $total_in_sent }}</b>/{{ $total_in }}</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-red" style="width:{{ $total_in_sent/$total_in*100 }}%"></div>
                    </div>
                  </div>
                  @endif
                  <!-- /.progress-group -->
                   @if ( $total_out) > 0 
                  <div class="progress-group">
                    <span class="progress-text">{{ trans('app.dashboard.report.monthly.goal.outcoming.title')}}</span>
                    <span class="progress-number"><b>{{ $total_out_sent }}</b>/{{ $total_out }}</span>

                    <div class="progress sm">
                      <div class="progress-bar progress-bar-green" style="width: {{ $total_out_sent/$total_out*100 }}%"></div>
                    </div>
                  </div>
                  @endif
                  <!-- /.progress-group -->
                </div>
                @endif
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- ./box-body -->
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
@endsection
<!-- ./wrapper -->

@push('scripts')

<!-- ChartJS 1.0.1 -->
<script src="{{ asset ('/bower_components/adminLTE/plugins/chartjs/Chart.min.js') }}"></script>

 <script>
        $(document).on('ready', function() {

          var options = {
              //Boolean - If we should show the scale at all
              showScale: true,
              //Boolean - Whether grid lines are shown across the chart
              scaleShowGridLines: false,
              //String - Colour of the grid lines
              scaleGridLineColor: "rgba(0,0,0,.05)",
              //Number - Width of the grid lines
              scaleGridLineWidth: 1,
              //Boolean - Whether to show horizontal lines (except X axis)
              scaleShowHorizontalLines: true,
              //Boolean - Whether to show vertical lines (except Y axis)
              scaleShowVerticalLines: true,
              //Boolean - Whether the line is curved between points
              bezierCurve: true,
              //Number - Tension of the bezier curve between points
              bezierCurveTension: 0.3,
              //Boolean - Whether to show a dot for each point
              pointDot: false,
              //Number - Radius of each point dot in pixels
              pointDotRadius: 4,
              //Number - Pixel width of point dot stroke
              pointDotStrokeWidth: 1,
              //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
              pointHitDetectionRadius: 20,
              //Boolean - Whether to show a stroke for datasets
              datasetStroke: true,
              //Number - Pixel width of dataset stroke
              datasetStrokeWidth: 2,
              //Boolean - Whether to fill the dataset with a color
              datasetFill: true,
              //String - A legend template
              legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",
              //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
              maintainAspectRatio: true,
              //Boolean - whether to make the chart responsive to window resizing
              responsive: true
            };

            $.ajax({
                url: "/postmetrics",
                data: {_token: "{!!csrf_token()!!}"},
                dataType: 'json',
                method: "post"


            }).done(function (data) {
                var ctx = document.getElementById("filesTxfrChart").getContext("2d");
                var myLineChart = new Chart(ctx).Line(data, options);
            });

        });

    </script>
@endpush
