@extends('layouts.admin', array('page_title' => trans('app.admin.settings.pagetitle'),
                                'breadcrumbs' => 'settings'))

@section('content')
     <div class="container-fluid">

    <div class="row">
      <div class="col-sm-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">{{ trans('app.admin.settings.formtitle')}}</h3>
          </div>
          <div class="panel-body">
    @include('partials.session_messages')

    <table class="table table-striped table-bordered">
	    <thead>
	    	<tr>
	     	  	<td>{{ trans('app.admin.settings.table.configitem')}}</td>
	     	  	<td>{{ trans('app.admin.settings.table.value')}}</td>
	     	  	<td>{{ trans('app.admin.settings.table.action')}}</td>
	     	</tr>
	    </thead>
     	<tbody>
		     @foreach ($configuration as $key => $value)
		    <tr>
		    	<td> {{ $key }} </td><
		    	<td> @if(is_bool($value) && $value)
		    			{{trans('app.admin.settings.yes')}}
		    		 @elseif (is_bool($value))
		    		    {{trans('app.admin.settings.no')}}
		    		 @else
		    		 	{{ $value }}
		    		 @endif
		    	</td>
		    	<td> @if (is_bool($value))
		    			<a href='/admin/settings/switch/{{$key}}' class='btn btn-xs btn-primary'><i class="fa fa-refresh"></i>&nbsp;{{ trans('app.admin.settings.button.switch')}}</a>
		    		 @endif
		    	</td>
		    </tr>
			@endforeach
		</tbody>
	</table>

</div>
        </div>
      </div>
    </div>
  </div>
@endsection
