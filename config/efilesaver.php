<?php

return [
	'manual_upload_files_in' => true,
	'manual_upload_files_out' => true,
	'transfer' => false,
	'mimic_transfer' => true,
	'overwrite_filename' => false,
	'storage' => 's3',
	'storage_base_folder' => 'uploads',
	'ftp_target_config' => 'sftp',
	'ftp_target_folder' => 'transfer/ocr',
	'ftp_source_config' => 'sftpget',
	'ftp_source_folder' => 'feedback',
	'accepted_files' => 'application/pdf,image/jpeg',
	'from_email' => 'info@efilesaver.org',
	'contact_email' => 'tanguylesseliers@gmail.com',
	'webpath' => 'https://s3-eu-west-1.amazonaws.com/efilesaver',

];
