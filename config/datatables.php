<?php

return [
	'search' => [
		'smart' => true,
		'case_insensitive' => true,
		'use_wildcards' => true,
	],

	'fractal' => [
		'serializer' => 'League\Fractal\Serializer\DataArraySerializer',
	],

	'script_template' => 'datatables::script',
];
