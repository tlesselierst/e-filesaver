<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileIn extends Model {
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['filename', 'filetype', 'filesize', 'origin', 'transfer_datestamp'];

	/**
	 * activating soft delete.
	 *
	 * @var boolean
	 */
	protected $softDelete = true;
	protected $table = 'files_in';
	protected $dates = ['transfer_datestamp'];
}
