<?php

namespace App\DataTables;

use App\FileOut;
use LaravelLocalization;
use Yajra\Datatables\Services\DataTable;

class FilesOutDataTable extends DataTable {
	// protected $printPreview  = 'path.to.print.preview.view';

	/**
	 * Display ajax response.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 *
	 **/
	public function ajax() {
		return $this->datatables
			->eloquent($this->query())
			->addColumn('operations', '<button type="button" class="btn btn-xs btn-danger" onclick="delete_file(\'{!!$filename!!}\', \'{!!$id!!}\')"> <i class="fa fa-times-circle fa-lg"></i>&nbsp;{{ trans(\'app.files.button.delete\')}}</button>')
			->editColumn('transfer_datestamp', function ($file) {
				return $file->transfer_datestamp->format('d/m/Y');
			})
			->editColumn('created_at', function ($file) {
				return $file->created_at->format('d/m/Y');
			})
			->editColumn('filesize', function ($file) {
				return human_filesize($file->filesize);
			})
			->make(true);
	}

	/**
	 * Get the query object to be processed by datatables.
	 *
	 * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
	 */
	public function query() {
		$files = FileOut::query();

		return $this->applyScopes($files);
	}

	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\Datatables\Html\Builder
	 */
	public function html() {

		// getting the correct language ...
		if (LaravelLocalization::getCurrentLocaleName() == 'Dutch') {
			$language_url = '//cdn.datatables.net/plug-ins/1.10.11/i18n/Dutch.json';
		} elseif (LaravelLocalization::getCurrentLocaleName() == 'French') {
			$language_url = '//cdn.datatables.net/plug-ins/1.10.11/i18n/French.json';
		} else {
			$language_url = '//cdn.datatables.net/plug-ins/1.10.11/i18n/English.json';
		}

		return $this->builder()
			->columns(
				[['data' => 'id', 'name' => 'id', 'title' => trans('app.files.table.id')],
					['data' => 'filename', 'name' => 'filename', 'title' => trans('app.filesOut.table.filename')],
					['data' => 'filetype', 'name' => 'filename', 'title' => trans('app.filesOut.table.filetype')],
					['data' => 'filesize', 'name' => 'filesize', 'title' => trans('app.filesOut.table.filesize')],
					['data' => 'created_at', 'name' => 'created_at', 'title' => trans('app.filesOut.table.created')],
					['data' => 'transfer_datestamp', 'name' => 'transfer_datestamp', 'title' => trans('app.filesOut.table.transferred')],
					['data' => 'operations', 'name' => 'operations', 'orderable' => false, 'searchable' => false, 'title' => trans('app.filesOut.table.actions')]])
			->parameters([
				'dom' => 'Blf<t>ip',
				'buttons' => ['csv', 'excel', 'print', 'reset', 'reload'],
				'language' => ["url" => $language_url],
				'colReorder' => 'true',
			]);

	}

	/**
	 * Get columns.
	 *
	 * @return array
	 */
	private function getColumns() {
		return [
			'id',
			'filename',
			'filetype',
			'filesize',
			'filepath',
			'transfer_datestamp',
			'created_at',

		];
	}

	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename() {
		return 'files_out';
	}
}
