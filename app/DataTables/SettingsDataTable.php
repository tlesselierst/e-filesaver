<?php

namespace App\DataTables;

use Config;
use Yajra\Datatables\Services\DataTable;

class SettingsDataTablej extends DataTable {
	// protected $printPreview  = 'path.to.print.preview.view';

	/**
	 * Display ajax response.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function ajax() {
		$settings = config('efilesaver');
		$data = new Collection($settings['items']);

		return Datatables::of($data)
			->filter(function () {}) // disable built-in search function
			->make(true);
	}

	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\Datatables\Html\Builder
	 */
	public function html() {
		return $this->builder()
			->columns($this->getColumns())
			->ajax('')
			->parameters($this->getBuilderParameters());
	}

	/**
	 * Get columns.
	 *
	 * @return array
	 */
	private function getColumns() {
		return [
			'key',
			'value',
		];
	}

	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename() {
		return 'settings';
	}
}
