<?php

namespace App\DataTables;
use App\User;
use LaravelLocalization;
use Yajra\Datatables\Services\DataTable;

class UsersDataTable extends DataTable {
	// protected $printPreview  = 'path.to.print.preview.view';

	/**
	 * Display ajax response.
	 *
	 * @return \Illuminate\Http\JsonResponse //id,name,email,user_type,api_token
	 */
	public function ajax() {
		return $this->datatables
			->eloquent($this->query())

			->addColumn('operations', '<button type="button" class="btn btn-xs btn-danger" onclick="delete_user(\'{!!$name!!}\', \'{!!$id!!}\')"> <i class="fa fa-times-circle fa-lg"></i>&nbsp;{{ trans(\'app.admin.users.button.delete\')}}</button>
                <a class="btn btn-xs btn-primary" href="{{ URL::to(\'admin/users/edit/\' . $id ) }}"><i class="fa fa-pencil"></i>&nbsp;{{ trans(\'app.admin.users.button.edit\')}} </a>')
			->make(true);
	}

	/**
	 * Get the query object to be processed by datatables.
	 *
	 * @return \Illuminate\Database\Query\Builder|\Illuminate\Database\Eloquent\Builder
	 */
	public function query() {
		$users = User::query();

		return $this->applyScopes($users);
	}

	/**
	 * Optional method if you want to use html builder.
	 *
	 * @return \Yajra\Datatables\Html\Builder
	 */
	public function html() {

		// getting the correct language ...
		if (LaravelLocalization::getCurrentLocaleName() == 'Dutch') {
			$language_url = '//cdn.datatables.net/plug-ins/1.10.11/i18n/Dutch.json';
		} elseif (LaravelLocalization::getCurrentLocaleName() == 'French') {
			$language_url = '//cdn.datatables.net/plug-ins/1.10.11/i18n/French.json';
		} else {
			$language_url = '//cdn.datatables.net/plug-ins/1.10.11/i18n/English.json';
		}

		return $this->builder()
			->columns(
				[['data' => 'id', 'name' => 'id', 'title' => trans('app.admin.users.table.id')],
					['data' => 'name', 'name' => 'name', 'title' => trans('app.admin.users.table.name')],
					['data' => 'email', 'name' => 'email', 'title' => trans('app.admin.users.table.email')],
					['data' => 'user_type', 'name' => 'user_type', 'title' => trans('app.admin.users.table.usertype')],
					['data' => 'api_token', 'name' => 'api_token', 'title' => trans('app.admin.users.table.api'), 'width' => '80px'],
					['data' => 'created_at', 'name' => 'created_at', 'title' => trans('app.admin.users.table.created')],
					['data' => 'operations', 'name' => 'operations', 'orderable' => false, 'searchable' => false, 'title' => trans('app.admin.users.table.action'), 'width' => '250px']])
			->parameters([
				'dom' => 'lf<t>ip',
				'language' => ["url" => $language_url],
			]);
	}

	/**
	 * Get columns.
	 *
	 * @return array
	 */
	private function getColumns() {
		return [
			'id',
			'name',
			'email',
			'user_type',
			'api_token',
			'created_at',
			'updated_at',
		];
	}

	/**
	 * Get filename for export.
	 *
	 * @return string
	 */
	protected function filename() {
		return 'users';
	}
}
