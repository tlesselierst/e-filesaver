<?php

namespace App\Jobs;

use App\FileOut;
use App\Jobs\Job;
use Carbon\Carbon;
use File;
use GuzzleHttp\Client;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;

class SendFileOutToUP extends Job implements ShouldQueue {
	use InteractsWithQueue, SerializesModels;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct() {

	}

	/**
	 * Execute the job.
	 * Specific part for integration with UnifiedPost,
	 * or any party accepting a post of a file to a simple Rest interface
	 *
	 * @return void
	 */
	public function handle() {
		$client = new Client(['http_errors' => false]);
		$toProcess = FileOut::ToProcess()->get();
		foreach ($toProcess as $file) {
			Log::info('starting the work for:' . $file['filename']);

			$storedfile = public_path('uploads/transfer/ocr/feedback/' . $file['filename']);
			$reference = basename($file['filename'], ".xml");

			$response = $client->put('https://ocr-uat-1-2.nxt.uat.unifiedpost.com/documents/' . $reference, [
				'multipart' => [
					[
						'name' => 'document',
						'contents' => fopen($storedfile, 'r'),
						'Content-Type' => 'text/xml',
					],
				],
				'auth' => [
					env('UP_REST_USER'),
					env('UP_REST_PWD'),
				],
			]);

			$statusReponse = $response->getStatusCode();

			if (200 === $statusReponse) {
				Log::info('Rest call to UP executed with response');
				FileOut::where('filename', $file['filename'])
					->update(['transfer_datestamp' => Carbon::now()]);

				File::delete($storedfile);

			} elseif (304 === $statusReponse) {
				// Nothing to doc- should not happen
			} elseif (404 === $statusReponse) {
				Log::info('UP informed that resource does not exist anymore. We can mark this file as synced');
				FileOut::where('filename', $file['filename'])
					->update(['transfer_datestamp' => Carbon::now()]);
				File::delete($storedfile);

			} else {
				Log::error('The sending of the xml to UP with the rest call went wrong');
			}
		}

	}
}
