<?php

namespace App\Jobs;

use App\FileIn;
use App\Jobs\Job;
use Carbon\Carbon;
use File;
use Flysystem;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;

class SendFileIn extends Job implements ShouldQueue {
	use InteractsWithQueue, SerializesModels;

	protected $filetoSend, $fileName;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($filetoSend, $fileName) {
		$this->filetoSend = $filetoSend;
		$this->fileName = $fileName;
	}

	/**
	 * Get the local file and transfer it to the
	 * target (s)ftp location
	 * then update the db
	 * //and delete the file ( not done yet)
	 *
	 * @return void
	 */

	public function handle() {
		if (file::exists($this->filetoSend)) {
			$contents = file::get($this->filetoSend);
			if (!config('efilesaver.mimic_transfer')) {
				Flysystem::connection(config('efilesaver.ftp_target_config'))->put(config('efilesaver.ftp_target_folder') . '/' . $this->fileName, $contents);
			} else {
				Log::info('TestFTP for ' . $this->fileName . '  executed.');
			}
			FileIn::where('filename', $this->fileName)
				->update(['transfer_datestamp' => Carbon::now()]);
		} else {
			Log::info('Job for ' . $this->fileName . ' not executed as the file could not be found.');
		}
		Log::info("FTP Sync executed");
	}
}
