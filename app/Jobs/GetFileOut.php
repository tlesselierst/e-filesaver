<?php

namespace App\Jobs;

use App\FileOut;
use App\Jobs\Job;
use File;
use Flysystem;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Log;

class GetFileOut extends Job implements ShouldQueue {
	use InteractsWithQueue, SerializesModels;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct() {
		//
	}

	/**
	 * Read the ftp  and get the files that are ready
	 * After transfer, the source file is deleted
	 * @return void
	 */
	public function handle() {
		$ftpcontent = Flysystem::connection(config('efilesaver.ftp_source_config'))->listContents('transfer/ocr/feedback', true);
		Log::info('Done reading FTP');
		foreach ($ftpcontent as $object) {
			Log::info($object['basename'] . ' is located at ' . $object['path'] . ' and is a ' . $object['type']);

			$FileOut = new FileOut;

			$FileOut->filename = $object['basename'];
			$FileOut->filetype = $object['type'];
			$FileOut->filesize = $object['size'];
			$FileOut->filepath = 'uploads/transfer/ocr/feedback';

			$FileOut->save();
			Log::info('file saved in database');

			$filecontent = Flysystem::connection(config('efilesaver.ftp_source_config'))->read('transfer/ocr/feedback/' . $object['basename']);
			$filetoSave = public_path('uploads/transfer/ocr/feedback/' . $object['basename']);
			File::put($filetoSave, $filecontent);
			$filecontent = null;
			Log::info('File saved in filesystem as: ' . $filetoSave);
			Flysystem::connection(config('efilesaver.ftp_source_config'))->delete('transfer/ocr/feedback/' . $object['basename']);
			Log::info('deleted from sftp');

		}

	}

}
