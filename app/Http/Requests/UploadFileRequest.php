<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class UploadFileRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $accepted_files = 'in:' . config('efilesaver.accepted_files');
        return [
             'file'=> 'required',
             'mime_type' => $accepted_files,
        ];
    }
}
