<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
 */

Route::group(['prefix' => LaravelLocalization::setLocale(),
	'middleware' => ['web', 'localeSessionRedirect']], function () {
	/* home other static pages */
	Route::get('/', 'PagesController@home');
	Route::get('support', 'PagesController@support');
	Route::post('contact', 'PagesController@sendContactInfo');

	/* page used in the front app to access files */
	Route::resource('filesIn', 'FilesInController');
	Route::resource('filesOut', 'FilesOutController');
	Route::get('readftp', 'FilesOutController@readftp');
	Route::get('synctoup', 'FilesOutController@synctoup');

	//admin pages ( need to be correctly route)
	Route::get('admin/dashboard', 'AdminController@dashboard');
	Route::any('/postmetrics', ['as' => 'post.metrics', 'uses' => 'AdminController@postMetrics']);
	Route::get('admin/settings/switch/{key}', 'AdminController@switchme');
	Route::get('admin/settings', 'AdminController@settings');

	//separate Useradmin
	Route::get('admin/users', 'UserAdminController@index');
	Route::get('admin/users/create', 'UserAdminController@create');
	Route::get('admin/users/{user}', 'UserAdminController@show');
	Route::get('profile', 'UserAdminController@profile');

	/* Managing Users */
	Route::get('admin/users/edit/{user}', ['as' => 'admin.users.edit', 'uses' => 'UserAdminController@edit']);

});

/* routes don't need to be translated*/
Route::group(['middleware' => 'web'], function () {
	/* created by the auth controller */
	//Route::auth();

	/*behind the screens */
	Route::post('filesIn', 'FilesInController@store');
	//Route::delete('filesIn/Delete/{files}', ['as' => 'filesIn.destroy', 'uses' => 'FilesInController@destroy']);
	Route::get('filesIn/download/{file}', 'FilesInController@getDownload');

	//Route::delete('filesOut/Delete/{files}', ['as' => 'filesOut.destroy', 'uses' => 'FilesOutController@destroy']);

	Route::delete('admin/users/{user}', ['as' => 'admin.user.destroy', 'uses' => 'UserAdminController@destroy']);
	Route::put('admin/users/{user}', ['as' => 'admin.users.update', 'uses' => 'UserAdminController@update']);
	Route::post('admin/users', 'UserAdminController@store');
	Route::delete('admin/avatar/{user}', 'UserAdminController@deleteAvatar');
});

/* api to post files */
Route::group(['prefix' => 'api/v1', 'middleware' => ['api', 'auth:api']], function () {
	Route::post('files', 'FilesRestController@store');
});
