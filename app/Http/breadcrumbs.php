<?php

// Home
Breadcrumbs::register('home', function ($breadcrumbs) {
	$breadcrumbs->push('Home', url('/'));
});

// Admin
Breadcrumbs::register('admin', function ($breadcrumbs) {
	$breadcrumbs->push('Admin');
});

// Admin > Dashboard
Breadcrumbs::register('dashboard', function ($breadcrumbs) {
	$breadcrumbs->parent('admin');
	$breadcrumbs->push('Dashboard', url('/admin/dashboard'));
});

// Admin > Settings
Breadcrumbs::register('settings', function ($breadcrumbs) {
	$breadcrumbs->parent('admin');
	$breadcrumbs->push('Settings', url('/admin/settings'));
});

// Admin > Users
Breadcrumbs::register('users', function ($breadcrumbs) {
	$breadcrumbs->parent('admin');
	$breadcrumbs->push('Users', url('/admin/users'));
});

// Admin > Users > Add
Breadcrumbs::register('users_add', function ($breadcrumbs) {
	$breadcrumbs->parent('users');
	$breadcrumbs->push('Add User', url('/admin/users/create'));
});

// Admin > Users > Edit
Breadcrumbs::register('user_edit', function ($breadcrumbs) {
	$breadcrumbs->parent('users');
	$breadcrumbs->push('Edit User ', url('/admin/users/edit/'));
});

// Admin > Users > Details
Breadcrumbs::register('user_detail', function ($breadcrumbs) {
	$breadcrumbs->parent('users');
	$breadcrumbs->push('User Details', url('/admin/users/edit/'));
});

// Admin
Breadcrumbs::register('files', function ($breadcrumbs) {
	$breadcrumbs->push('Files');
});

// Admin > Files In
Breadcrumbs::register('files_in', function ($breadcrumbs) {
	$breadcrumbs->parent('files');
	$breadcrumbs->push('Files In', url('/admin/files/'));
});

// Admin > Files Out
Breadcrumbs::register('files_out', function ($breadcrumbs) {
	$breadcrumbs->parent('files');
	$breadcrumbs->push('Files Out', url('/admin/feedbackfiles/'));
});