<?php

namespace App\Services;

use Dflydev\ApacheMimeTypes\PhpRepository;
use Illuminate\Support\Facades\Storage;
use Log;

class UploadsManager {
	protected $disk;
	protected $mimeDetect;

	public function __construct(PhpRepository $mimeDetect) {
		$this->mimeDetect = $mimeDetect;
	}

	/**
	 *Return Files and directories within a folder
	 *
	 * @param string $folder
	 * @return array of [
	 *   'folder' => 'path to current folder',
	 *   'folderName' => 'name of the current folder',
	 *    'breadcrumbs' => breadcrumb array of [
	 *     	$path => $foldername]
	 *   'folders' => array of [ $path => $foldername] of each subfolder
	 *   'files' => array of file details on each file in folder
	 *   ]
	 */

	/**
	 * Sanitize the foldername
	 */

	protected function cleanFolder($folder) {
		return  trim(str_replace('..', '', $folder), '/');
	}

	/*
		* return an array of file details for a file
	*/

	public function fileDetails($path) {
		$path = ltrim($path, '/');

		return [
			'name' => basename($path),
			'fullPath' => $path,
			'mimeType' => $this->fileMimeType($path),
			'size' => $this->fileSize($path),
			'target_fs' => config('efilesaver.storage')
		];
	}

	/**
	 * return the full path to a file
	 */

	public function fileWebpath($path) {
		$path = rtrim(config('efilesaver.webpath'), '/') . '/' . ltrim($path, '/');
		return url($path);
	}

	/**
	 * return the MimeType
	 */

	public function fileMimeType($path) {
		return $this->mimeDetect->findType(pathinfo($path, PATHINFO_EXTENSION));
	}

	/*
		* return filesize
	*/

	public function fileSize($path) {
		return $this->disk->size($path);
	}

	/*
		* return last modified  date
	*/

	/**
	 * Save a file
	 */
	public function saveFile($path, $content) {
		  $path = $this->cleanFolder($path);
		  $this->disk = Storage::disk(config('efilesaver.storage'));

		if ($this->disk->exists($path)) {
			return trans('messages.filesIn.file_exists');
		}

		return $this->disk->put($path, $content);
	}

	/**
	 * Delete a file
	 */
	public function deleteFile($path, $disk) {
		$path = $this->cleanFolder($path);
		$this->disk = Storage::disk($disk ?: config('efilesaver.storage'));

		if (!$this->disk->exists($path)) {
			return "File does not exist.";
		}

		return $this->disk->delete($path);
	}

	/**
	 * Return a file
	 */
	public function getFile($path,  $disk) {
		$path = $this->cleanFolder($path);
		$this->disk = Storage::disk($disk ?: config('efilesaver.storage'));

		if (!$this->disk->exists($path)) {
			return "File does not exist.";
		}

		return $this->disk->get($path);
	}

}