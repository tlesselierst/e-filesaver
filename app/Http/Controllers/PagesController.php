<?php

namespace App\Http\Controllers;
use App\Http\Requests\ContactMeRequest;
use Illuminate\Support\Facades\Mail;

class PagesController extends Controller {
	public function home() {
		return view('pages.home');
	}

	public function support() {
		return view('pages.support');
	}

	public function sendContactInfo(ContactMeRequest $request) {
		$data = $request->only('name', 'email', 'phone');
		$data['messageLines'] = explode("\n", $request->get('message'));

		Mail::queue('emails.contact', $data, function ($message) use ($data) {
			$message->subject('eFileSaver contact Form: ' . $data['name'])
				->to(config('efilesaver.contact_email'))
				->from(config('efilesaver.from_email'))
				->replyTo($data['email']);
		});

		return back()
			->withSuccess(trans('messages.help.thank_you'));
	}
}
