<?php

namespace App\Http\Controllers;

use App\FileIn;
use App\Http\Requests\UploadFileRequest;
use App\Jobs\SendFileIn;
use App\Services\UploadsManager;
use File;
use Log;
use Response;

class FilesRestController extends Controller {
	/*
		  * Create new instance of the FileRestController
		  * all resources here can only be accessed if authenticated.
	*/
	public function __construct(UploadsManager $manager) {
		$this->manager = $manager;
	}

	/**
	 * Store a file.
	 *
	 */
	public function store(UploadFileRequest $request) {
		$file = $_FILES['file'];

		//Definining the name of the file :
		// random, original or optional name
		if (config('efilesaver.overwrite_filename')) {
			$extension = $request->file('file')->getClientOriginalExtension(); // getting file extension
			$fileName = rand(11111, 99999) . '.' . $extension; // renaming file
		} else {
			$fileName = $request->get('file_name');
			$fileName = $fileName ?: $file['name'];
		}

		$path = str_finish(config('efilesaver.storage_base_folder'), '/') . $fileName;
		$content = File::get($file['tmp_name']);

		$result = $this->manager->saveFile($path, $content);

		if ($result === true) {

			$FileInfo = $this->manager->fileDetails($path);
			//Instantiate an instance of the file model and fill it;
			$FileIn = new FileIn;

			$FileIn->filename = $FileInfo['name'];
			$FileIn->filetype = $FileInfo['mimeType'];
			$FileIn->filesize = $FileInfo['size'];
			$FileIn->origin = 'REST';
			$FileIn->file_path = $FileInfo['fullPath'];

			$FileIn->save();

			if (config('efilesaver.transfer')) {
				$filetoSend = public_path($FileInfo['fullPath']);
				Log::info('scheduling job for : ' . $fileName . ' accessible as ' . $filetoSend);
				$job = new SendFileIn($filetoSend, $fileName);
				$this->dispatch($job);
			}

			// sending back with message
			return Response::json(['message' => 'File Uploaded'], 200);
		}

		// sending back with error message.
		return Response::json(['Error' => 'uploaded file is not valid or missing'], 400);
	}
}