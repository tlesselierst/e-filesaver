<?php

namespace App\Http\Controllers;

use App\DataTables\UsersDataTable;
use App\User;
use Illuminate\Http\Request;
use Redirect;
use Session;
use Validator;
use Auth;
use Image;
use Hash;
use Storage;
use Log;

class UserAdminController extends Controller {
	//All methods only after authentication
	public function __construct() {
		$this->middleware('auth');
	}

	public function index(UsersDataTable $dataTable) {

		return $dataTable->render('admin.users.index');

	}

	public function show($id) {
		// get the user
		$user = User::find($id);

		// show the view and pass the user to it
		return view('admin.users.detail')->with('user', $user);
	}

	public function profile(){
		return view('admin.users.edit')->with('user',  Auth::user());
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create() {
		// load the create form (app/views/admin.users/create.blade.php)
		return view('admin.users.create');
	}

	public function edit($id) {
		// get the user
		$user = User::find($id);

		// show the edit form and pass the user
		return view('admin.users.edit')->with('user', $user);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request) {
		// validate
		// read more on validation at http://laravel.com/docs/validation
		$rules = array(
			'name' => 'required',
			'email' => 'required|email',
			'user_type' => 'required',
		);
		$validator = Validator::make($request->all(), $rules);

		// process the login
		if ($validator->fails()) {
			return Redirect::to('admin/users/create')
				->withErrors($validator)
				->withInput($request->except('password'));
		} else {
			// store

			$user = new User;

			if($request->hasFile('avatar')){
    			$avatar = $request->file('avatar');
    			$filename = time() . '.' . $avatar->getClientOriginalExtension();
    			Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/avatars/' . $filename ) );
    			$user->avatar = $filename;
    		} 	

    		if(!empty($request->password)) {
    			$user->password = Hash::make($request->password);
    		}	

			$user->name = $request->name;
			$user->email = $request->email;
			$user->user_type = $request->user_type;
			if ($request->has('api_token')) {
				$user->api_token = $request->api_token;
			} else {
				$user->api_token = str_random(8);
			}
			$user->save();

			// redirect
			Session::flash('success', trans('messages.user_created'));
			return Redirect::to('admin/users');
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request) {
		// validate
		// read more on validation at http://laravel.com/docs/validation
		$rules = array(
			'name' => 'required',
			'email' => 'required|email',
			'user_type' => 'required',
		);
		$validator = Validator::make($request->all(), $rules);

		// process the login
		if ($validator->fails()) {
			return Redirect::to('admin.users/' . $id . '/edit')->withErrors($validator)->withInput(Input::except('password'));
		} else {
			// store
			$user = User::find($id);
			if($request->hasFile('avatar')){
				$avatar_file= $user->avatar ;
				if(!empty($avatar_file) ) {
					Storage::disk('local')->delete('uploads/avatars/'.$avatar_file);
				}
    			$avatar = $request->file('avatar');
    			$filename = time() . '.' . $avatar->getClientOriginalExtension();
    			Image::make($avatar)->resize(300, 300)->save( public_path('uploads/avatars/' . $filename ) );
    			$user->avatar = $filename;
    		} 

    		if(!empty($request->password)) {
    			$user->password = Hash::make($request->password);
    		}

			$user->name = $request->name;
			$user->email = $request->email;
			$user->user_type = $request->user_type;
			if ($request->has('api_token')) {
				$user->api_token = $request->api_token;
			}
			$user->save();

			// redirect
			Session::flash('success', trans('messages.user_updated'));
			return Redirect::to('admin/users');
		}
	}

	/**
	 * Remove the avatar of the user.
	 *
	 * @param  int  $id  
	 * @return Response
	 */
	public function deleteAvatar($id) {
		// delete
		$user = User::find($id);
		$avatar_file= $user->avatar ;
		if(!empty($avatar_file) ) {
			Storage::disk('local')->delete('uploads/avatars/'.$avatar_file);
			$user->avatar = '';
			$user->save();

			// redirect
			Session::flash('success', trans('messages.avatar_deleted'));
		} 
    	
    	return Redirect::to('admin/users');

		
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id) {
		// delete
		$user = User::find($id);
		$user->delete();

		// redirect
		Session::flash('success', trans('messages.user_deleted'));
		return Redirect::to('admin/users');
	}

}
