<?php

namespace App\Http\Controllers;

use App\DataTables\FilesOutDataTable;
use App\FileOut;
use App\Jobs\GetFileOut;
use App\Jobs\SendFileOutToUP;
use File;
use Illuminate\Http\Request;
use Log;
use Redirect;
use Session;

class FilesOutController extends Controller {
	/*
		        * Create new instance of the FileFrontController
		        * all resources here can only be accessed if authenticated.
	*/
	public function __construct() {
		$this->middleware('auth');
		$this->store_path = 'uploads/transfer/ocr/feedback';
	}

	/**
	 *
	 * show all uploaded or received files
	 *
	 */
	public function index(FilesOutDataTable $dataTable) {

		//show the create button ?
		$allow_create_fb = config('efilesaver.manual_upload_files_out');
		$data = array('allow_create_fb' => $allow_create_fb);
		return $dataTable->render('filesOut.index', $data);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		// check that the request an expected file
		if ($request->hasFile('file')) {

			$realfile = $request->file('file');
			$file = array('file' => $realfile,
				'mime_type' => $request->file('file')->getClientMimeType(),
			);

			$file = array_add($file, 'filename', $request->file('file')->getClientOriginalName());
			$file = array_add($file, 'filesize', $request->file('file')->getClientSize());
			$file = array_add($file, 'origin', 'UI');

			//preparing to save the physical file
			$extension = $request->file('file')->getClientOriginalExtension(); // getting image extension
			$fileName = $file['filename'];
			$fileToCheck = $this->store_path . '/' . $fileName;

			if (File::exists($fileToCheck)) {
				// sending back with error message.
				return Redirect::to('filesOut')->withInput()->withErrors(trans('messages.filesIn.file_exists'));
			}

			$file = array_add($file, 'filepath', $this->store_path);

			//Instantiate an instance of the file model and fill it;
			$FileOut = new FileOut;

			$FileOut->filename = $file['filename'];
			$FileOut->filetype = $file['mime_type'];
			$FileOut->filesize = $file['filesize'];
			//$FileOut->origin = $file['origin'];
			$FileOut->filepath = $file['filepath'];

			//saving in both folder and database
			$request->file('file')->move($this->store_path, $fileName); // uploading file to given path
			$FileOut->save();

			// sending back with message
			Session::flash('success', trans('messages.filesIn.file_upload_ok'));
			return Redirect::to('filesOut');
		} else {
			// sending back with error message.
			return Redirect::to('filesOut')->withInput()->withErrors(trans('messages.filesIn.file_not_ok'));
		}
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		$FileOut = Fileout::findOrFail($id);

		$physical_file = $this->store_path . '/' . $FileOut->filename;

		File::delete($physical_file);

		$FileOut->delete();

		return redirect()->route('filesOut.index')->with('success', trans('messages.file_deleted'));
	}

	/*
		*
		* function is starting the ftp to get the content...
		*
	*/
	public function readftp() {

		$jobSyncFTP = new GetFileOut();
		$this->dispatch($jobSyncFTP);
		Log::info('FTP synchro requested');
		return redirect('filesOut')->with('success', trans('messages.ftpsync_requested'));
	}

	public function synctoup() {
		$jobSyncToUP = new SendFileOutToUP();
		$this->dispatch($jobSyncToUP);
		Log::info('Launched request to post to UP');
		return redirect('filesIn')->with('success', trans('messages.synctoUP_requested'));
	}
}
