<?php

namespace App\Http\Controllers;
use App\FileIn as Incoming;
use App\FileOut as Outgoing;
use Carbon\Carbon;
use Config;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Session;

class AdminController extends Controller {

	public function __construct() {
		$this->middleware('auth');
	}

	/**
	 * Show the Settings screen based on the efilesaver config file
	 *
	 * @return
	 */

	public function settings() {
		$configuration = config('efilesaver');
		return view('admin.settings', compact('configuration'));
	}

	/**
	 * Retrieve stats from the DB and show the dasboard view
	 *
	 * @return
	 */

	public function dashboard() {
		//Getting the statistics for the dashboard. Simple counts
		$total_in = Incoming::count();
		$total_in_sent = Incoming::where('transfer_datestamp', '!=', '0000-00-00 00:00:00')->count();
		$total_out = Outgoing::count();
		$total_out_sent = Outgoing::where('transfer_datestamp', '!=', '0000-00-00 00:00:00')->count();

		$min1 = Incoming::all()->min('created_at');
		$min2 = Outgoing::all()->min('created_at');
		$min = min($min1, $min2);
		$min = Carbon::parse($min)->format('d M Y');

		$max1 = Incoming::all()->max('created_at');
		$max2 = Outgoing::all()->max('created_at');
		$max = max($max1, $max2);
		$max = Carbon::parse($max)->format('d M Y');

		//add to to the array that will be loaded in the view
		$data = [];
		$data['total_in'] = $total_in;
		$data['total_in_sent'] = $total_in_sent;
		$data['total_out'] = $total_out;
		$data['total_out_sent'] = $total_out_sent;
		$data['min'] = $min;
		$data['max'] = $max;

		return view('admin.dashboard', $data);
	}

	/**
	 * Retrieve and return the dashboard view/comments metrics
	 *
	 * @return JSON
	 */
	public function postMetrics() {

		//select query to return totals by month for incoming files
		$totals = array();
		$totals = DB::table('files_in')
			->select(DB::raw('YEAR(created_at) as Year'), DB::raw('MONTH(created_at) as Month'), DB::raw('count(*) as filecount'))
			->groupBy('Year', 'Month')
			->get();
		$totals = new Collection($totals);

		//select query to return totals by month for incoming files
		$totalsout = array();
		$totalsout = DB::table('files_out')
			->select(DB::raw('YEAR(created_at) as Year'), DB::raw('MONTH(created_at) as Month'), DB::raw('count(*) as filecount'))
			->groupBy('Year', 'Month')
			->get();
		$totalsout = new Collection($totalsout);

		//initialises the arrays to avoid working on non defined variables.
		$labels = array();
		$fileInDataset = array();
		$fileOutDataset = array();

		//Getting translated labels for the months
		$labels = [trans('app.jan'),
			trans('app.feb'),
			trans('app.mar'),
			trans('app.apr'),
			trans('app.may'),
			trans('app.jun'),
			trans('app.jul'),
			trans('app.aug'),
			trans('app.sep'),
			trans('app.oct'),
			trans('app.nov'),
			trans('app.dec'),
		];

		/**
		 *  creating the array used by the graph
		 *  For each month, set the number of files
		 *  and set the value to 0 if the queery did
		 *  not return a value for that month
		 */

		for ($x = 0; $x <= 11; $x++) {
			if ($totals->contains('Month', $x + 1)) {
				$test = $totals->where('Month', $x + 1);
				$result = $test->first()->filecount;
				$fileInDataset[$x] = $result;

			} else {
				$fileInDataset[$x] = 0;
			}
		}

		for ($x = 0; $x <= 11; $x++) {
			if ($totalsout->contains('Month', $x + 1)) {
				$testout = $totalsout->where('Month', $x + 1);
				$result = $testout->first()->filecount;
				$fileOutDataset[$x] = $result;

			} else {
				$fileOutDataset[$x] = 0;
			}
		}

		$viewData = array('labels' => $labels, 'datasets' => array(
			array('label' => "Incoming Files",
				'fillColor' => "rgb(221, 75, 57)",
				'strokeColor' => "rgb(221, 75, 57)",
				'pointColor' => "#DE4B39",
				'pointStrokeColor' => "#DE4B39",
				'pointHighlightFill' => "#fff",
				'pointHighlightStroke' => "rgb(221, 75, 57)",
				'data' => $fileInDataset),
			array('label' => "Outgoing Files",
				'fillColor' => "rgba(0,160,90,0.9)",
				'strokeColor' => "rgba(0,160,90,0.8)",
				'pointColor' => "#00A05A",
				'pointStrokeColor' => "rgba(60,141,188,1)",
				'pointHighlightFill' => "#fff",
				'pointHighlightStroke' => "rgba(0,160,90,1)",
				'data' => $fileOutDataset),

		));
		return (json_encode($viewData));
	}

	/**
	 * function to swhitch a true/false setting in the efilesaver config file
	 *
	 * @return JSON
	 */

	public function switchme($key) {

		$thekey = 'efilesaver.' . $key;

		if (config($thekey)) {
			$newvalue = false;
			$newvalue_display = 'No';
		} else {
			$newvalue = true;
			$newvalue_display = 'Yes';
		}
		Config::write('efilesaver', [$key => $newvalue]);
		Session::flash('success', trans('messages.changed_params', ['key' => $key, 'newvalue' => $newvalue_display]));
		return redirect('admin/settings');
	}
}
