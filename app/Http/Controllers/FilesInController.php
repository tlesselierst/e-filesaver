<?php

namespace App\Http\Controllers;

use App\DataTables\FilesInDataTable;
use App\FileIn;
use App\Jobs\SendFileIn;
use File;
use Illuminate\Http\Request;
use Log;
use Redirect;
use Session;
use Validator;
use App\Services\UploadsManager;
use Storage;

class FilesInController extends Controller {

	/**
	 * Create new instance of the FileInController
	 * all resources here can only be accessed if authenticated.
	 *
	 **/
	public function __construct(UploadsManager $manager) {
		$this->middleware('auth');
		$this->store_path = config('efilesaver.storage_base_folder');
		$this->overwrite_filename = config('efilesaver.overwrite_filename');
		$this->manager = $manager;
	}

	/**
	 *
	 * List Files In (uploaded or through rest call)
	 *
	 **/
	public function index(FilesInDataTable $dataTable) {

		//check to see if manual upload button must be shown
		$allow_create = config('efilesaver.manual_upload_files_in');
		$data = array('allow_create' => $allow_create);

		//render through use of datatable
		return $dataTable->render('filesIn.index', $data);
	}

	/**
	 *
	 * Show detail of a file
	 *
	 */
	public function show($id) {

		$record = FileIn::find($id);
		return view('filesIn.show')->with('record', $record);
	}

	/**
	 *
	 * Store a file.
	 *
	 */
	public function store(Request $request) {

		if ($request->hasFile('file')) {
			$accepted_files = 'mimetypes:' . config('efilesaver.accepted_files');
			$this->validate($request, [
        		'file' => $accepted_files,
        	]);

			//determine filename 
			$extension = $request->file('file')->getClientOriginalExtension(); // getting image extension
			if ($this->overwrite_filename) {
				$fileName = rand(11111, 99999) . '.' . $extension; // renaming image
			} else {
				if ($request->file_name) {
					$fileName = $request->file_name;
				} else {
					$fileName =  $request->file('file')->getClientOriginalName();
				}

				/*$fileToCheck = $this->store_path . '/' . $fileName;
				if (File::exists($fileToCheck)) {
					// sending back with error message if file exists
					return Redirect::to('filesIn')->withInput()->withErrors(trans('messages.filesIn.file_exists'));
				}*/
			}

			/*//saving in both folder and database
			$request->file('file')->move($this->store_path, $fileName);*/

			$path = str_finish(config('efilesaver.storage_base_folder'), '/') . $fileName;
			$content = File::get($request->file('file'));

			$result = $this->manager->saveFile($path, $content);

			if ($result === true) {

				$FileInfo = $this->manager->fileDetails($path);
				//Instantiate an instance of the file model and fill it;
				$FileIn = new FileIn;

				$FileIn->filename = $FileInfo['name'];
				$FileIn->filetype = $FileInfo['mimeType'];
				$FileIn->filesize = $FileInfo['size'];
				$FileIn->origin = 'UI';
				$FileIn->file_path = $FileInfo['fullPath'];
				$FileIn->target_fs = $FileInfo['target_fs'];

				$FileIn->save();

				//
				if (config('efilesaver.transfer')) {
					$filetoSend = public_path($this->store_path . '/' . $fileName);
					Log::info('scheduling job for : ' . $fileName . ' accessible as ' . $filetoSend);
					$job = new SendFileIn($filetoSend, $fileName);
					$this->dispatch($job);
				}

				// sending back with ok message
				Session::flash('success', trans('messages.filesIn.file_upload_ok'));
				return Redirect::to('filesIn');
			}

			// sending back with error message if file exists
			return Redirect::to('filesIn')->withInput()->withErrors(trans('messages.filesIn.file_exists'));

        } else {
        	// sending back with error message.
			return Redirect::to('filesIn')->withInput()->withErrors(trans('messages.filesIn.file_not_ok'));
        }	
	}

	public function destroy($id) {
		$file = FileIn::findOrFail($id);
		$Result = $this->manager->deleteFile($file->file_path, $file->target_fs);

		if ($Result) {
			$file->delete();
		
			Session::flash('success', trans('messages.filesIn.file_delete_ok'));
			return redirect()->route('filesIn.index');

		} else {
			return Redirect::to('filesIn')->withInput()->withErrors(trans('messages.filesIn.file_delete_not_ok'));
		}

	
	}


	public function getDownload($id) {

		$file = FileIn::findOrFail($id);
		$path = $file->file_path;
	
		//$File = $this->manager->getFile($path);
		if (config('efilesaver.storage') === 's3') {
			$File = $path;
		} else {
			$File = storage_path('app/public') . "/" . $path;
		}

		$FileContent = $this->manager->getFile($path, $file->target_fs);

		$response = response($FileContent, 200, [
            'Content-Type' => $file->filetype,
            'Content-Length' => $file->filesize,
            'Content-Description' => 'File Transfer',
            'Content-Disposition' => 'attachment; filename=' .$file->filename,
            'Content-Transfer-Encoding' => 'binary',
        ]);

        ob_end_clean(); // <- this is important, i have forgotten why.

        return $response;

	}
}