<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileOut extends Model {
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['filename', 'filetype', 'filesize', 'filepath', 'transfer_datestamp'];

	/**
	 * activating soft delete.
	 *
	 * @var boolean
	 */
	protected $softDelete = true;

	protected $dates = ['transfer_datestamp'];

	protected $table = 'files_out';

	public function scopeToProcess($query) {
		return $query->where('transfer_datestamp', '0000-00-00 00:00:00');
	}
}
