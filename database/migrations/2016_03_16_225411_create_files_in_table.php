<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFilesInTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files_in', function (Blueprint $table) {
            $table->increments('id');
            $table->string('filename');
            $table->string('filetype');
            $table->integer('filesize');
            $table->string('origin');
            $table->string('file_path');
            $table->string('target_fs', 20);
            $table->dateTime('transfer_datestamp');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('files_in');
    }
}
