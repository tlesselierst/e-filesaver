<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFilesOutTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('files_out', function (Blueprint $table) {
			$table->increments('id');
			$table->string('filename');
			$table->string('filetype');
			$table->integer('filesize');
			$table->string('filepath');
			$table->string('target_fs', 20);
			$table->dateTime('transfer_datestamp');
			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::drop('feedbackfiles');
	}
}
