# e-FileSaVeR

[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

This is an application with a backend to receive files through http request and a frontend to manage the received files

Powered by Laravel

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).